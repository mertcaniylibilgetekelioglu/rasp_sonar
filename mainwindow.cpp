#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    wiringPiSetup();
    sonar.init(trigger,echo);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::move() {
   setPointX(sonar.distance(30000));
   QWidget::update();
}

void MainWindow::paintEvent(QPaintEvent *event)
{
     Q_UNUSED(event);
    //-------------------------------------
    QPainter myrect(this);
    QPen rectPen(Qt::blue);
    rectPen.setWidth(5);
    myrect.setPen(rectPen);
    myrect.drawRect(20,50,70,200);
    myrect.drawEllipse(QRect(30,70,50,70));
    myrect.drawEllipse(QRect(30,160,50,70));
    // Bu bölüm hc-sr04 şemasını çizdiriyor
    //--------------------------------------
    QPainter myline(this);
    QPen linePen2(Qt::green);
    linePen2.setWidth(5);
    myline.setPen(linePen2);
    for(int i=1;i<7;i++)
    {
        myline.drawLine((60*i)+40,270,(60*i)+40,300);

    }
    myline.drawLine(100,300,400,300);
    //Bu bölüm mesafeleri belirten çizgileri çizdiriyor
    //---------------------------
    QPainter text(this);
    text.setPen(Qt::black);
    QString s;
    for(int i=0;i<6;i++)
    {
        s=QString::number(10*i);
        text.drawText((60*(i+1))+40,260,(s+"cm."));
    }
    //Çizgilere karşılık gelen cm değerleri
    //Buradan sonra HC-SR04 tarafından algılanan nesnenin çizimi
    /*sonar.init(trigger,echo);
    int dist=sonar.distance(30000);
*/
    if(dist<50)
    {
        QPainter obj(this);
        QPen objpen(Qt::red);
        objpen.setWidth(10);
        obj.setPen(objpen);
        obj.drawRect((100+(getPointX()*6)),140,10,10);
    }

}
void MainWindow::keyPressEvent(QKeyEvent *event)
{
    move();
}
int MainWindow::getPointX() const
{
    return dist;
}
void MainWindow::setPointX(int distance)
{
    dist=distance;
}



